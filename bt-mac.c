#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <libqrtr.h>
#include "msm_ipc.h"
#include "qmi_dms.h"

int main(void)
{
	struct sockaddr_msm_ipc sa_ipc;
	struct dms_get_mac_resp resp;
	struct dms_get_mac_req req = {
		.mac_type = QMI_DMS_MAC_TYPE_BT,
	};
	struct {
		struct server_lookup_args lookup;
		struct msm_ipc_server_info server;
	} args = {
		.lookup = {
			.num_entries_in_array = 1,
			.port_name = {
				.service = 2, /* Device Management Service */
				.instance = 1,
			},
			.lookup_mask = 0xffffffff,
		}
	};
	uint8_t /* btbuf[260], */ btcmd[] = {
		0x0b, 0xfc,	/* command opcode */
		9,		/* command length */
		1,		/* set tag */
		2,		/* tag id */
		6,		/* tag value length */
		/* tag value: reversed bdaddr */
		0x22, 0x22, 0x22, 0x22, 0x22, 0x22,
	};
	DEFINE_QRTR_PACKET(cmd_buf, 256);
	DEFINE_QRTR_PACKET(resp_buf, 256);
	unsigned int txn = 1;
	int fd, ret;

	fd = socket(AF_MSM_IPC, SOCK_DGRAM, 0);
	if(fd < 0){
		perror("Failed to create socket(AF_MSM_IPC)");
		return 1;
	}
	fprintf(stderr, "Getting server info...\n");
	if(ioctl(fd, IPC_ROUTER_IOCTL_LOOKUP_SERVER, &args.lookup) < 0){
		perror("Failed to get server info using ioctl");
		close(fd);
		return 1;
	}
	fprintf(stderr,
		"Found %d servers for service %d, instance %d.\n",
		args.lookup.num_entries_found, args.lookup.port_name.service,
		args.lookup.port_name.instance);
	if(args.lookup.num_entries_found <= 0){
		close(fd);
		return 1;
	}
	sa_ipc.family = AF_MSM_IPC;
	sa_ipc.address.addrtype = MSM_IPC_ADDR_ID;
	sa_ipc.address.addr.port_addr.node_id = args.server.node_id;
	sa_ipc.address.addr.port_addr.port_id = args.server.port_id;

	ret = qmi_encode_message(&cmd_buf, QMI_REQUEST, QMI_DMS_GET_MAC,
		txn, &req, dms_get_mac_req_ei);
	if(ret < 0){
		fprintf(stderr, "Failed to encode MAC request: %s\n",
			strerror(-ret));
		close(fd);
		return 1;
	}

	ret = sendto(fd, cmd_buf.data, ret, MSG_DONTWAIT,
		(struct sockaddr*)&sa_ipc, sizeof(sa_ipc));
	if(ret < 0){
		perror("Failed to send MAC request");
		close(fd);
		return 1;
	}

	ret = recvfrom(fd, resp_buf.data, resp_buf.data_len, 0, NULL, NULL);
	if(ret < 0){
		perror("Failed to recvfrom()");
		close(fd);
		return 1;
	}

	close(fd);

	ret = qmi_decode_message(&resp, &txn, &resp_buf, QMI_RESPONSE,
		QMI_DMS_GET_MAC, dms_get_mac_resp_ei);
	if(ret < 0){
		fprintf(stderr, "Failed to decode MAC response: %s\n",
			strerror(-ret));
		return 1;
	}
	if(resp.mac_addr_len != 6){
		fprintf(stderr, "MAC address size is not 6 bytes!\n");
		return 1;
	}

	printf("MAC address is %02x:%02x:%02x:%02x:%02x:%02x\n",
		resp.mac_addr[0], resp.mac_addr[1], resp.mac_addr[2],
		resp.mac_addr[3], resp.mac_addr[4], resp.mac_addr[5]);

	fd = open("/dev/smd3", O_RDWR);
	if(fd < 0 && errno == EBUSY){
		fprintf(stderr,
			"BT driver in use, set MAC on next boot\n");
		return 0;
	}
	else if(fd < 0){
		perror("Failed to open smd3 (bluetooth control channel)");
		return 1;
	}

	btcmd[sizeof(btcmd) - 1] = resp.mac_addr[0];
	btcmd[sizeof(btcmd) - 2] = resp.mac_addr[1];
	btcmd[sizeof(btcmd) - 3] = resp.mac_addr[2];
	btcmd[sizeof(btcmd) - 4] = resp.mac_addr[3];
	btcmd[sizeof(btcmd) - 5] = resp.mac_addr[4];
	btcmd[sizeof(btcmd) - 6] = resp.mac_addr[5];
	ret = write(fd, &btcmd, sizeof(btcmd));
	if(ret < 0){
		perror("Failed to write BDADDR");
		close(fd);
		return 1;
	}
	/* We might need this read later, but it's useless right now */
	/* read(fd, &btbuf, sizeof(btbuf)); */
	close(fd);
	return 0;
}
