CFLAGS = -g
PREFIX =
OBJECTS = bootmodem.o
W_OBJECTS = wcnss-mac.o qmi_dms.o
B_OBJECTS = bt-mac.o qmi_dms.o
R_OBJECTS = rmnet_config.o
RV_OBJECTS = rmnet_data.o
LIBS = libqipcrtr4msmipc.so libsmdpkt_wrapper.so
LDFLAGS_QRTR = -lqrtr
LDFLAGS_MNL = -lmnl
OUTPUTS = bootmodem bootmodem.service wcnss-mac wcnss-mac.service qcom-bt-mac qcom-bt-mac.service rmnet-setdatamode rmnet-createvnd $(LIBS)

all: $(OUTPUTS)

%.so: %.c
	$(CC) $(CFLAGS) -fPIC -std=c99 -Wall -Wextra -Werror -shared $< -o $@ -ldl

bootmodem: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

wcnss-mac: $(W_OBJECTS)
	$(CC) $(W_OBJECTS) -o $@ $(LDFLAGS) $(LDFLAGS_QRTR)

qcom-bt-mac: $(B_OBJECTS)
	$(CC) $(B_OBJECTS) -o $@ $(LDFLAGS) $(LDFLAGS_QRTR)

rmnet-setdatamode: $(R_OBJECTS)
	$(CC) $(R_OBJECTS) -o $@ $(LDFLAGS) $(LDFLAGS_QRTR)

rmnet-createvnd: $(RV_OBJECTS)
	$(CC) $(RV_OBJECTS) -o $@ $(LDFLAGS) $(LDFLAGS_QRTR) $(LDFLAGS_MNL)

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

wcnss-mac.o: qmi_dms.h
bt-mac.o: qmi_dms.h

%.service: %.service.in
	sed 's+BOOTMODEM_PATH+$(PREFIX)/bin+g' $< > $@

install: $(OUTPUTS)
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/lib/systemd/system
	install -m 755 bootmodem $(DESTDIR)$(PREFIX)/bin
	install -m 644 bootmodem.service $(DESTDIR)$(PREFIX)/lib/systemd/system
	install -m 755 wcnss-mac $(DESTDIR)$(PREFIX)/bin
	install -m 644 wcnss-mac.service $(DESTDIR)$(PREFIX)/lib/systemd/system
	install -m 755 qcom-bt-mac $(DESTDIR)$(PREFIX)/bin
	install -m 644 qcom-bt-mac.service $(DESTDIR)$(PREFIX)/lib/systemd/system
	install -m 755 rmnet-createvnd $(DESTDIR)$(PREFIX)/bin
	install -m 755 rmnet-setdatamode $(DESTDIR)$(PREFIX)/bin
	install -m 755 $(LIBS) $(DESTDIR)$(PREFIX)/lib

clean:
	rm -rf $(OUTPUTS) $(W_OBJECTS) $(OBJECTS)
