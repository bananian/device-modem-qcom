CFLAGS = -g
PREFIX =
OBJECTS = bootmodem.o
LIBS = libqipcrtr4msmipc.so libsmdpkt_wrapper.so

all: $(LIBS) bootmodem.service bootmodem

%.so: %.c
	$(CC) $(CFLAGS) -fPIC -std=c99 -Wall -Wextra -Werror -shared $< -o $@

bootmodem: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

bootmodem.service: bootmodem.service.in
	sed 's+BOOTMODEM_PATH+$(PREFIX)/bin+g' $< > $@

install: bootmodem bootmodem.service
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/lib/systemd/system
	install -m 755 bootmodem $(DESTDIR)$(PREFIX)/bin
	install -m 644 bootmodem.service $(DESTDIR)$(PREFIX)/lib/systemd/system
	install -m 755 $(LIBS) $(DESTDIR)$(PREFIX)/lib

clean:
	rm -rf bootmodem.service bootmodem $(OBJECTS) $(LIBS)
