#ifndef __QMI_DMS_H__
#define __QMI_DMS_H__

#include <stdint.h>
#include <stdbool.h>

#include "libqrtr.h"

#define QMI_DMS_RESULT_SUCCESS 0
#define QMI_DMS_RESULT_FAILURE 1
#define QMI_DMS_ERR_NONE 0
#define QMI_DMS_ERR_INTERNAL 1
#define QMI_DMS_ERR_MALFORMED_MSG 2
#define QMI_DMS_GET_MAC 92
#define QMI_DMS_MAC_TYPE_WLAN 0
#define QMI_DMS_MAC_TYPE_BT 1

struct dms_qmi_result {
	uint16_t result;
	uint16_t error;
};

struct dms_get_mac_req {
	uint32_t mac_type;
};

struct dms_get_mac_resp {
	struct dms_qmi_result result;
	bool mac_addr_valid;
	uint32_t mac_addr_len;
	uint8_t mac_addr[6];
};

extern struct qmi_elem_info dms_get_mac_req_ei[];
extern struct qmi_elem_info dms_get_mac_resp_ei[];

#endif
