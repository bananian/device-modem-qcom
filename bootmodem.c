#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include "msm_ipc.h"

#ifndef BOOTFILEPATH
#define BOOTFILEPATH "/sys/kernel/boot_adsp/boot"
#endif

/* The QMI commands, taken from strace logs of netmgrd. Will need some reverse
 * engineering.
 */
static const char buf1[] = "\0\1\0\"\0\0";
static const char buf2[] = "\0\2\0 \0+\0\20\24\0\1\nDATA5_CNTL\5\0\0\0\0\0\0\0\21\21\0\1\5\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

int main()
{
	FILE *bootfile;
	struct server_lookup_args *sla;
	struct sockaddr_msm_ipc sa_ipc;
	unsigned char dummybuf[4096];
	socklen_t sa_ipc_len = sizeof(sa_ipc);
	int sock, i;
	fprintf(stderr, "Writing 1 to " BOOTFILEPATH "...\n");
	bootfile = fopen(BOOTFILEPATH, "w");
	if(!bootfile){
		perror("Failed to open boot file");
		return 1;
	}
	fputs("1\n", bootfile);
	fclose(bootfile);
	fprintf(stderr, "Opening IPC router...\n");
	sock = socket(AF_MSM_IPC, SOCK_DGRAM, 0);
	if(sock < 0){
		perror("Failed to create socket(AF_MSM_IPC)");
		return 1;
	}
	sleep(10);
	fprintf(stderr, "Getting server info...\n");
	sla = malloc(sizeof(struct server_lookup_args) +
		sizeof(struct msm_ipc_server_info));
	sla->num_entries_in_array = 1;
	sla->port_name.service = 0x2f;
	sla->port_name.instance = 0x1;
	sla->lookup_mask = 0xffffffff;
	if(ioctl(sock, IPC_ROUTER_IOCTL_LOOKUP_SERVER, sla) < 0){
		perror("Failed to get server info using ioctl");
		close(sock);
		return 1;
	}
	fprintf(stderr,
		"Found %d servers for service %d, instance %d.\n",
		sla->num_entries_found, sla->port_name.service,
		sla->port_name.instance);
	if(sla->num_entries_found <= 0){
		close(sock);
		return 1;
	}
	sa_ipc.family = AF_MSM_IPC;
	sa_ipc.address.addrtype = MSM_IPC_ADDR_ID;
	sa_ipc.address.addr.port_addr.node_id = sla->srv_info[0].node_id;
	sa_ipc.address.addr.port_addr.port_id = sla->srv_info[0].port_id;
	/*fprintf(stderr, "Waiting for modem message...\n");
	if(recvfrom(sock, dummybuf, sizeof(dummybuf), 0,
		(struct sockaddr*)&sa_ipc, &sa_ipc_len) < 0)
	{
		perror("Failed to recvfrom()");
		close(sock);
		return 1;
	}*/
	fprintf(stderr, "Sending command...\n");
	if(fork() == 0){
		int sock2;
		struct config_sec_rules_args rules = {
			.num_group_info = 0,
			.service_id = 0,
			.instance_id = 0
		};
		fprintf(stderr, "[child] Connecting to IPC router...\n");
		sock2 = socket(AF_MSM_IPC, SOCK_DGRAM, 0);
		if(sock2 < 0){
			perror("[child] Failed to connect to IPC router");
			exit(0);
		}
		fprintf(stderr, "[child] Setting security rules...\n");
		if(ioctl(sock2, IPC_ROUTER_IOCTL_CONFIG_SEC_RULES, &rules)){
			perror("[child] Failed to set security rules");
		}
		close(sock2);
		exit(0);
	}
	if(sendto(sock, buf1, sizeof(buf1), MSG_DONTWAIT,
		(struct sockaddr*)&sa_ipc, sa_ipc_len) < 0)
	{
		perror("Failed to send");
		close(sock);
		return 1;
	}
	fprintf(stderr, "Sending command to open DATA5_CNTL...\n");
	if(sendto(sock, buf2, sizeof(buf2), MSG_DONTWAIT,
		(struct sockaddr*)&sa_ipc, sa_ipc_len) < 0)
	{
		perror("Failed to send");
		close(sock);
		return 1;
	}
	return 0;
}
