#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include "msm_rmnet.h"

int main(int argc, char * const *argv)
{
	int sk = socket(AF_PACKET, SOCK_RAW, 0);
	struct rmnet_ioctl_data_s getopmode = {
		.u = { .operation_mode = RMNET_MODE_LLP_IP },
	};
	struct rmnet_ioctl_extended_s getepid = {
		.extended_ioctl = RMNET_IOCTL_GET_EPID,
	};
	struct ifreq req = {};
	if(argc > 2 && argv[1][0] != '-'){
		strncpy(req.ifr_name, argv[1], IFNAMSIZ-1);
		req.ifr_name[IFNAMSIZ-1] = '\0';
		if(0 == strcmp(argv[2], "ip")){
			getopmode.u.operation_mode = RMNET_MODE_LLP_IP;
		}
		else if(0 == strcmp(argv[2], "ethernet")){
			getopmode.u.operation_mode = RMNET_MODE_LLP_ETH;
		}
		else {
			fprintf(stderr, "protocol must be ip or ethernet\n");
			return 1;
		}
	}
	else {
		printf("Usage: rmnet-setdatamode <interface> <protocol>\n"
			"where phys_interface is the name of an "
			"rmnet interface and protocol is \n"
			"either `ip` or `ethernet` and matches "
			"the modem's setting.\n");
		return 1;
	}
	if(sk < 0){
		perror("socket");
		return 2;
	}
	req.ifr_data = NULL;
	if(ioctl(sk, RMNET_IOCTL_SET_LLP_IP, &req)){
		perror("ioctl (set opmode)");
		return 2;
	}
	req.ifr_data = (__caddr_t)&getepid;
	if(ioctl(sk, RMNET_IOCTL_EXTENDED, &req)){
		perror("ioctl (get ep)");
		return 2;
	}
	printf("%d\n", getepid.u.data);
	return 0;
}
