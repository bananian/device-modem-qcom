#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <libqrtr.h>
#include "msm_ipc.h"
#include "qmi_dms.h"

#define WCNSS_CTRL_SERIAL_NUM 1
#define WCNSS_CTRL_HAS_CAL_DATA 2
#define WCNSS_CTRL_WLAN_MAC_ADDR 3

int main(void)
{
	struct sockaddr_msm_ipc sa_ipc;
	struct dms_get_mac_resp resp;
	struct dms_get_mac_req req = {
		.mac_type = QMI_DMS_MAC_TYPE_WLAN,
	};
	struct {
		uint8_t cmd[2];
		uint8_t mac_addr[6];
	} __attribute__((__packed__)) wct_mac = {
		.cmd = { 0, WCNSS_CTRL_WLAN_MAC_ADDR },
	};
	struct {
		uint8_t cmd[2];
		uint8_t has_cal_data;
	} __attribute__((__packed__)) wct_cal = {
		.cmd = { 0, WCNSS_CTRL_HAS_CAL_DATA },
	};
	struct {
		struct server_lookup_args lookup;
		struct msm_ipc_server_info server;
	} args = {
		.lookup = {
			.num_entries_in_array = 1,
			.port_name = {
				.service = 2, /* Device Management Service */
				.instance = 1,
			},
			.lookup_mask = 0xffffffff,
		}
	};
	DEFINE_QRTR_PACKET(cmd_buf, 256);
	DEFINE_QRTR_PACKET(resp_buf, 256);
	unsigned int txn = 1;
	int fd, calfd, ret;

	if(access("/run/wcnss_loaded.stamp", R_OK) == 0){
		fprintf(stderr,
			"WCNSS already loaded, set MAC on next boot\n");
		return 0;
	}

	fd = socket(AF_MSM_IPC, SOCK_DGRAM, 0);
	if(fd < 0){
		perror("Failed to create socket(AF_MSM_IPC)");
		return 1;
	}
	fprintf(stderr, "Getting server info...\n");
	if(ioctl(fd, IPC_ROUTER_IOCTL_LOOKUP_SERVER, &args.lookup) < 0){
		perror("Failed to get server info using ioctl");
		close(fd);
		return 1;
	}
	fprintf(stderr,
		"Found %d servers for service %d, instance %d.\n",
		args.lookup.num_entries_found, args.lookup.port_name.service,
		args.lookup.port_name.instance);
	if(args.lookup.num_entries_found <= 0){
		close(fd);
		return 1;
	}
	sa_ipc.family = AF_MSM_IPC;
	sa_ipc.address.addrtype = MSM_IPC_ADDR_ID;
	sa_ipc.address.addr.port_addr.node_id = args.server.node_id;
	sa_ipc.address.addr.port_addr.port_id = args.server.port_id;

	ret = qmi_encode_message(&cmd_buf, QMI_REQUEST, QMI_DMS_GET_MAC,
		txn, &req, dms_get_mac_req_ei);
	if(ret < 0){
		fprintf(stderr, "Failed to encode MAC request: %s\n",
			strerror(-ret));
		close(fd);
		return 1;
	}

	ret = sendto(fd, cmd_buf.data, ret, MSG_DONTWAIT,
		(struct sockaddr*)&sa_ipc, sizeof(sa_ipc));
	if(ret < 0){
		perror("Failed to send MAC request");
		close(fd);
		return 1;
	}

	ret = recvfrom(fd, resp_buf.data, resp_buf.data_len, 0, NULL, NULL);
	if(ret < 0){
		perror("Failed to recvfrom()");
		close(fd);
		return 1;
	}

	close(fd);

	ret = qmi_decode_message(&resp, &txn, &resp_buf, QMI_RESPONSE,
		QMI_DMS_GET_MAC, dms_get_mac_resp_ei);
	if(ret < 0){
		fprintf(stderr, "Failed to decode MAC response: %s\n",
			strerror(-ret));
		return 1;
	}
	if(resp.mac_addr_len != 6){
		fprintf(stderr, "MAC address size is not 6 bytes!\n");
		return 1;
	}

	printf("MAC address is %02x:%02x:%02x:%02x:%02x:%02x\n",
		resp.mac_addr[0], resp.mac_addr[1], resp.mac_addr[2],
		resp.mac_addr[3], resp.mac_addr[4], resp.mac_addr[5]);

	fd = open("/dev/wcnss_ctrl", O_WRONLY);
	if(fd < 0){
		perror("Failed to open /dev/wcnss_ctrl");
		return 1;
	}

	memcpy(wct_mac.mac_addr, resp.mac_addr, 6);
	ret = write(fd, &wct_mac, sizeof(wct_mac));
	if(ret < 0){
		perror("Failed to write MAC address to /dev/wcnss_ctrl");
		close(fd);
		return 1;
	}
	wct_cal.has_cal_data = 0;
	ret = write(fd, &wct_cal, sizeof(wct_cal));
	if(ret < 0){
		perror("Failed to write has_cal_data to /dev/wcnss_ctrl");
		close(fd);
		return 1;
	}
	close(fd);

	fd = open("/dev/wcnss_wlan", O_RDWR);
	if(fd < 0){
		perror("Failed to open /dev/wcnss_wlan");
		return 1;
	}
	close(fd);

	fd = open("/run/wcnss_loaded.stamp", O_WRONLY|O_CREAT|O_TRUNC, 0600);
	if(fd < 0){
		perror("Failed to create wcnss_loaded stamp");
		return 1;
	}
	close(fd);
	return 0;
}
