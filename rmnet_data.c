#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <libmnl/libmnl.h>
#include "rmnet_data.h"

int main(int argc, char * const *argv)
{
	struct mnl_socket *sk;
	struct nlmsghdr *hdr;
	struct rmnet_nl_msg_s *rhdr;
	void *buf;
	size_t size, rsize = sizeof(struct rmnet_nl_msg_s);
	const char *phys_interface;
	if(argc > 1 && argv[1][0] != '-'){
		phys_interface = argv[1];
	}
	else {
		printf("Usage: rmnet-createvnd <phys_interface>\n"
			"where phys_interface is the name of an "
			"existing network interface used to\n"
			"send and receive packets.\n");
		return 1;
	}
	sk = mnl_socket_open2(31, SOCK_NONBLOCK);
	if(!sk){
		perror("socket");
		return 2;
	}
	if(mnl_socket_bind(sk, 0, 0) < 0){
		perror("bind");
		return 2;
	}
	size = mnl_nlmsg_size(rsize);
	buf = calloc(1, size);
	hdr = mnl_nlmsg_put_header(buf);
	hdr->nlmsg_pid = getpid();
	hdr->nlmsg_len += rsize;
	rhdr = mnl_nlmsg_get_payload(hdr);
	rhdr->message_type = RMNET_NETLINK_NEW_VND;
	rhdr->vnd.id = 0;
	/*fprintf(stderr, "TX:\n");
	mnl_nlmsg_fprintf(stderr, buf, size, 0);*/
	if(mnl_socket_sendto(sk, buf, size) < 0){
		perror("sendto");
		return 2;
	}
	while(1){
		if(mnl_socket_recvfrom(sk, buf, size) < 0){
			if(errno == EAGAIN){
				sleep(1);
				continue;
			}
			perror("recvfrom");
			return 2;
		}
		break;
	}
	fprintf(stderr, "RX:\n");
	mnl_nlmsg_fprintf(stderr, buf, size, 0);

	memset(rhdr, 0, rsize);
	hdr = mnl_nlmsg_put_header(buf);
	hdr->nlmsg_pid = getpid();
	hdr->nlmsg_len += rsize;
	rhdr->message_type = RMNET_NETLINK_ASSOCIATE_NETWORK_DEVICE;
	strncpy(rhdr->data, phys_interface, RMNET_NL_DATA_MAX_LEN-1);
	rhdr->data[RMNET_NL_DATA_MAX_LEN-1] = '\0';
	/*fprintf(stderr, "TX:\n");
	mnl_nlmsg_fprintf(stderr, buf, size, 0);*/
	if(mnl_socket_sendto(sk, buf, size) < 0){
		perror("sendto");
		return 2;
	}
	while(1){
		if(mnl_socket_recvfrom(sk, buf, size) < 0){
			if(errno == EAGAIN){
				sleep(1);
				continue;
			}
			perror("recvfrom");
			return 2;
		}
		break;
	}
	if(rhdr->crd == RMNET_NETLINK_MSG_RETURNDATA){
		fprintf(stderr, "OK", rhdr->return_code);
	}
	else {
		fprintf(stderr, "Assoc error: %u\n", rhdr->return_code);
	}

	memset(rhdr, 0, rsize);
	hdr = mnl_nlmsg_put_header(buf);
	hdr->nlmsg_pid = getpid();
	hdr->nlmsg_len += rsize;
	rhdr->message_type = RMNET_NETLINK_SET_LOGICAL_EP_CONFIG;
	strncpy(rhdr->local_ep_config.dev, phys_interface, RMNET_MAX_STR_LEN-1);
	rhdr->local_ep_config.dev[RMNET_MAX_STR_LEN-1] = '\0';
	rhdr->local_ep_config.ep_id = RMNET_LOCAL_LOGICAL_ENDPOINT;
	rhdr->local_ep_config.operating_mode = RMNET_EPMODE_VND;
	strcpy(rhdr->local_ep_config.next_dev, "rmnet_data0");
	/*fprintf(stderr, "TX:\n");
	mnl_nlmsg_fprintf(stderr, buf, size, 0);*/
	if(mnl_socket_sendto(sk, buf, size) < 0){
		perror("sendto");
		return 2;
	}
	while(1){
		if(mnl_socket_recvfrom(sk, buf, size) < 0){
			if(errno == EAGAIN){
				sleep(1);
				continue;
			}
			perror("recvfrom");
			return 2;
		}
		break;
	}
	if(rhdr->crd == RMNET_NETLINK_MSG_RETURNDATA){
		fprintf(stderr, "OK", rhdr->return_code);
	}
	else {
		fprintf(stderr, "Epconfig error: %u\n", rhdr->return_code);
	}

	memset(rhdr, 0, rsize);
	hdr = mnl_nlmsg_put_header(buf);
	hdr->nlmsg_pid = getpid();
	hdr->nlmsg_len += rsize;
	rhdr->message_type = RMNET_NETLINK_SET_LOGICAL_EP_CONFIG;
	strcpy(rhdr->local_ep_config.dev, "rmnet_data0");
	rhdr->local_ep_config.ep_id = RMNET_LOCAL_LOGICAL_ENDPOINT;
	rhdr->local_ep_config.operating_mode = RMNET_EPMODE_VND;
	strncpy(rhdr->local_ep_config.next_dev, phys_interface,
			RMNET_MAX_STR_LEN-1);
	rhdr->local_ep_config.next_dev[RMNET_MAX_STR_LEN-1] = '\0';
	/*fprintf(stderr, "TX:\n");
	mnl_nlmsg_fprintf(stderr, buf, size, 0);*/
	if(mnl_socket_sendto(sk, buf, size) < 0){
		perror("sendto");
		return 2;
	}
	while(1){
		if(mnl_socket_recvfrom(sk, buf, size) < 0){
			if(errno == EAGAIN){
				sleep(1);
				continue;
			}
			perror("recvfrom");
			return 2;
		}
		break;
	}
	if(rhdr->crd == RMNET_NETLINK_MSG_RETURNDATA){
		fprintf(stderr, "OK", rhdr->return_code);
	}
	else {
		fprintf(stderr, "Epconfig error: %u\n", rhdr->return_code);
	}
	return 0;
}
