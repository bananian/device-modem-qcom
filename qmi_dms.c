#include <errno.h>
#include <string.h>
#include "qmi_dms.h"

struct qmi_elem_info dms_qmi_result_ei[] = {
	{
		.data_type = QMI_UNSIGNED_2_BYTE,
		.elem_len = 1,
		.elem_size = sizeof(uint16_t),
		.offset = offsetof(struct dms_qmi_result, result),
	},
	{
		.data_type = QMI_UNSIGNED_2_BYTE,
		.elem_len = 1,
		.elem_size = sizeof(uint16_t),
		.offset = offsetof(struct dms_qmi_result, error),
	},
	{}
};

struct qmi_elem_info dms_get_mac_req_ei[] = {
	{
		.data_type = QMI_UNSIGNED_4_BYTE,
		.elem_len = 1,
		.elem_size = sizeof(uint32_t),
		.tlv_type = 1,
		.offset = offsetof(struct dms_get_mac_req, mac_type),
	},
	{}
};

struct qmi_elem_info dms_get_mac_resp_ei[] = {
	{
		.data_type = QMI_STRUCT,
		.elem_len = 1,
		.elem_size = sizeof(struct dms_qmi_result),
		.tlv_type = 2,
		.offset = offsetof(struct dms_get_mac_resp, result),
		.ei_array = dms_qmi_result_ei,
	},
	{
		.data_type = QMI_OPT_FLAG,
		.elem_len = 1,
		.elem_size = sizeof(bool),
		.tlv_type = 16,
		.offset = offsetof(struct dms_get_mac_resp, mac_addr_valid),
	},
	{
		.data_type = QMI_DATA_LEN,
		.elem_len = 1,
		.elem_size = sizeof(uint8_t),
		.tlv_type = 16,
		.offset = offsetof(struct dms_get_mac_resp, mac_addr_len),
	},
	{
		.data_type = QMI_UNSIGNED_1_BYTE,
		.elem_len = 6,
		.elem_size = sizeof(uint8_t),
		.array_type = VAR_LEN_ARRAY,
		.tlv_type = 16,
		.offset = offsetof(struct dms_get_mac_resp, mac_addr),
	},
	{}
};

